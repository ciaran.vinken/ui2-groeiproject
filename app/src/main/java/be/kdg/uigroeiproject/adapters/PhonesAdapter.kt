package be.kdg.uigroeiproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.model.Phone
import kotlinx.android.synthetic.main.phone_list_item.view.*

class PhonesAdapter(
    private val phones: Array<Phone>
) : RecyclerView.Adapter<PhonesAdapter.PhoneViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val phoneView = inflater.inflate(R.layout.phone_list_item, parent, false)
        return PhoneViewHolder(phoneView)
    }

    override fun getItemCount(): Int {
       return phones.size
    }

    override fun onBindViewHolder(holder: PhoneViewHolder, position: Int) {
        val phone = phones[position]
        holder.model.text = phone.model
        holder.ram.text = phone.ram
    }

    class PhoneViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val model: TextView = view.model
        val ram: TextView = view.ram

    }

}


