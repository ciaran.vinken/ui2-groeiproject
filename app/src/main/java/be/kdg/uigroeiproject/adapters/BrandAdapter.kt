package be.kdg.uigroeiproject.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.model.SmartphoneBrand
import be.kdg.uigroeiproject.model.convertDate
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_recycler_item.view.*

class BrandAdapter(
    private var brands: Array<SmartphoneBrand>,
    private val context: Context,
    private val brandSelectionListener: BrandSelectionListener
) : RecyclerView.Adapter<BrandAdapter.BrandViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.activity_recycler_item, parent, false)
        return BrandViewHolder(view)
    }

    override fun getItemCount() = brands.size

    override fun getItemViewType(position: Int): Int {
        return R.layout.activity_recycler_item
    }

    override fun onBindViewHolder(vh: BrandViewHolder, position: Int) {
        val observable = RestClient(context)
            .getBrand(position)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { brand ->
                vh.logo.setImageBitmap(brand.imageBitmap)
                vh.brand.text = brand.brand_name
                vh.date.text = brand.founding_date
                vh.years.text = brand.yearsService.toString()
            }
        vh.itemView.setOnClickListener {
            brandSelectionListener.onBrandSelected(position)
        }
    }


    interface BrandSelectionListener {
        fun onBrandSelected(position: Int)
    }

    class BrandViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        val brand: TextView = view.item_brand
        val date: TextView = view.item_date
        val years: TextView = view.item_years
        val logo: ImageView = view.item_logo
    }


}