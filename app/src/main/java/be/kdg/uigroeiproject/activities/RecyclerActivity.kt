package be.kdg.uigroeiproject.activities

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.adapters.BrandAdapter
import be.kdg.uigroeiproject.fragments.DetailFragment
import be.kdg.uigroeiproject.fragments.RecyclerFragment
import be.kdg.uigroeiproject.model.SmartphoneBrand
import be.kdg.uigroeiproject.model.getData
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RecyclerActivity : AppCompatActivity(), BrandAdapter.BrandSelectionListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)
    }


    override fun onBrandSelected(position: Int) {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("currentIndex", position)
            startActivity(intent)
        } else {
            val detailFrag = supportFragmentManager
                .findFragmentById(R.id.detail_land_frag)
                    as DetailFragment
            detailFrag.setBrandIndex(position)
        }
    }


}
