package be.kdg.uigroeiproject.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.adapters.PhonesAdapter
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PhonesRecycler : AppCompatActivity() {
    private lateinit var disposable: Disposable
    private lateinit var rvPhones: RecyclerView
    private var currentIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phones_recycler)
        currentIndex = intent.getIntExtra("currentIndex", 0)
        initializeViews()

        disposable = RestClient(this)
            .getBrand(currentIndex)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { brand ->
                val disp = RestClient(this)
                    .getPhones(brand.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { phones ->
                        rvPhones.apply {
                            layoutManager = LinearLayoutManager(this@PhonesRecycler)
                            adapter = PhonesAdapter(phones)
                        }
                        this.title = brand.brand_name
                    }
            }

    }


    private fun initializeViews() {
        rvPhones = findViewById(R.id.rvPhones)

    }

    override fun onDestroy() {
        if (::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }
}
