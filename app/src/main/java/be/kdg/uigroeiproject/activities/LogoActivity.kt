package be.kdg.uigroeiproject.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.model.getData
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class LogoActivity : AppCompatActivity() {
    private lateinit var logo: ImageView
    private var currentIndex: Int = 0
    private lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logo)

        currentIndex = intent.getIntExtra("currentIndex", 0)
        displayItem()
    }


    private fun displayItem() {
        logo = findViewById(R.id.expanded_logo)

        disposable = RestClient(this@LogoActivity)
            .getBrand(currentIndex)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { brand ->
                this.logo.setImageBitmap(brand.imageBitmap)
            }

    }

    override fun onDestroy() {
        if (::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }
}
