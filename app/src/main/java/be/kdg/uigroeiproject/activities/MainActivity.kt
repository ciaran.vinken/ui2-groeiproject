package be.kdg.uigroeiproject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.adapters.PhonesAdapter
import be.kdg.uigroeiproject.fragments.DetailFragment
import be.kdg.uigroeiproject.model.SmartphoneBrand
import be.kdg.uigroeiproject.model.convertDate
import be.kdg.uigroeiproject.model.getData
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private lateinit var logo: ImageView
    private lateinit var brandName: TextView
    private lateinit var foundingDate: TextView
    private lateinit var yearsService: TextView
//    private lateinit var nextButton: Button
//    private lateinit var previousButton: Button
    private lateinit var phonesButton: Button
    private lateinit var disposable: Disposable

    private var currentIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currentIndex = intent.getIntExtra("currentIndex", 0)

        initializeViews()
        addEventHandlers()
        displayItem()
    }

    private fun initializeViews() {
        logo = findViewById(R.id.imgLogo)
        brandName = findViewById(R.id.brand_value)
        foundingDate = findViewById(R.id.date_value)
        yearsService = findViewById(R.id.years_value)
        phonesButton = findViewById(R.id.phones_button)
    }

    private fun nextItem() {
        if (currentIndex < 14) {
            currentIndex++
            displayItem()
        } else {
            currentIndex = 14
            displayItem()
        }
    }

    private fun previousItem() {
        if (currentIndex > 0) {
            currentIndex--
            displayItem()
        } else {
            currentIndex = 0
            displayItem()
        }
    }


    private fun addEventHandlers() {
//        nextButton = findViewById(R.id.next_button)
//        previousButton = findViewById(R.id.previous_button)
//        nextButton.setOnClickListener {
//            nextItem()
//        }
//        previousButton.setOnClickListener { previousItem() }

        phonesButton.setOnClickListener {
            val phonesIntent = Intent(this, PhonesRecycler::class.java)
            phonesIntent.putExtra("currentIndex", currentIndex)
            startActivity(phonesIntent)
        }
        logo.setOnClickListener {
            val intent = Intent(this, LogoActivity::class.java)
            intent.putExtra("currentIndex", currentIndex)
            startActivity(intent)
        }
    }


    private fun displayItem() {
        disposable = RestClient(this@MainActivity)
            .getBrand(currentIndex)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { brand ->
                this.logo.setImageBitmap(brand.imageBitmap)
                this.brandName.text = brand.brand_name
                this.foundingDate.text = brand.founding_date
                this.yearsService.text = brand.yearsService.toString()
                this.phonesButton.setOnClickListener() {
                    val phonesIntent = Intent(this, PhonesRecycler::class.java)
                    phonesIntent.putExtra("currentIndex", currentIndex)
                    startActivity(phonesIntent)
                }
            }
    }

    override fun onDestroy() {
        if (::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }
}
