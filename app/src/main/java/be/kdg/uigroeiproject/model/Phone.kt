package be.kdg.uigroeiproject.model

data class Phone(
    val id: Int,
    val model: String,
    val brand: String,
    val ram: String,
    val brandId: Int
) {

}