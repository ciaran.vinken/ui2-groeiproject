package be.kdg.uigroeiproject.model


import android.graphics.Bitmap
import be.kdg.uigroeiproject.R
import java.util.*

data class SmartphoneBrand(
    val id: Int,
    val brand_name: String,
//    val founding_date: GregorianCalendar,
    val founding_date: String,
    val logo: String,
    val yearsService: Int,
    var imageBitmap : Bitmap
) {}