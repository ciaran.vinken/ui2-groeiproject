package be.kdg.uigroeiproject.model

import be.kdg.uigroeiproject.R
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

fun getData(): Array<SmartphoneBrand> {
    return arrayOf(
//        SmartphoneBrand(1, "OnePlus", GregorianCalendar(2013, Calendar.DECEMBER, 16), R.drawable.oneplus_logo, 6,null),
//        SmartphoneBrand(2, "Samsung", GregorianCalendar(1938, Calendar.MARCH, 1), R.drawable.samsung_logo, 81,null),
//        SmartphoneBrand(3, "Xiaomi", GregorianCalendar(2010, Calendar.APRIL, 6), R.drawable.xiaomi_logo, 9,null),
//        SmartphoneBrand(4, "Nokia", GregorianCalendar(1865, Calendar.MAY, 12), R.drawable.nokia_logo, 154,null),
//        SmartphoneBrand(5, "Huawei", GregorianCalendar(1987, Calendar.SEPTEMBER, 15), R.drawable.huawei_logo, 31,null),
//        SmartphoneBrand(6, "LG", GregorianCalendar(1958, Calendar.DECEMBER, 1), R.drawable.lg_logo, 61,null),
//        SmartphoneBrand(
//            7, "Motorola", GregorianCalendar(1928, Calendar.SEPTEMBER, 25), R.drawable.motorola_logo, 91,null
//        ),
//        SmartphoneBrand(8, "Google", GregorianCalendar(2004, Calendar.AUGUST, 19), R.drawable.google_logo, 15,null),
//        SmartphoneBrand(9, "Oppo", GregorianCalendar(2001,Calendar.JANUARY, 1), R.drawable.oppo_logo, 19,null),
//        SmartphoneBrand(10, "HTC", GregorianCalendar(1997,Calendar.MAY, 15), R.drawable.htc_logo, 22,null),
//        SmartphoneBrand(11, "Acer", GregorianCalendar(1976, Calendar.AUGUST, 1), R.drawable.acer_logo, 43,null),
//        SmartphoneBrand(12, "Asus", GregorianCalendar(1989, Calendar.APRIL, 2), R.drawable.asus_logo, 30,null),
//        SmartphoneBrand(13, "Lenovo", GregorianCalendar(1984, Calendar.NOVEMBER, 1), R.drawable.lenovo_logo, 35,null),
//        SmartphoneBrand(14, "Sony", GregorianCalendar(1946, Calendar.JANUARY, 1), R.drawable.sony_logo, 73,null),
//        SmartphoneBrand(15, "Toshiba", GregorianCalendar(1875, Calendar.JULY, 1), R.drawable.toshiba_logo, 144,null)
    )
}
fun convertDate(date:GregorianCalendar):String{
    val formatter = SimpleDateFormat("dd/MM/yyyy",Locale.US)
    println(date.time)
    return formatter.format(date.time)
}