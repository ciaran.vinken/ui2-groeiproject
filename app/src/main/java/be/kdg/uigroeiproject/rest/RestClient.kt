package be.kdg.uigroeiproject.rest

import android.content.Context
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import be.kdg.uigroeiproject.model.Phone
import be.kdg.uigroeiproject.model.SmartphoneBrand
import com.google.gson.GsonBuilder
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import io.reactivex.Observable

const val BASE_URL = "http://10.0.2.2:3000"

class RestClient(private val context: Context) {


    @Suppress("DEPRECATION")
    private fun connect(urlString: String): HttpURLConnection {
        val connMgr = context.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            val url = URL(urlString)
            val connection = url.openConnection() as HttpURLConnection
            connection.apply {
                connectTimeout = 15000
                readTimeout = 10000
                requestMethod = "GET"
                connect()
            }
            return connection
        }
        throw IOException("Unable to connect to network")
    }

    fun getBrand(id: Int): Observable<SmartphoneBrand> {
        val obs = Observable.create<SmartphoneBrand> { emitter ->
            try {
                var connection = connect("${BASE_URL}/brands/${id + 1}")
                val gson = GsonBuilder().create()


                val brand = gson.fromJson(
                    InputStreamReader(connection.inputStream),
                    SmartphoneBrand::class.java
                )
                brand.apply {
                    connection = connect("${BASE_URL}/${brand.logo}")
                    brand.imageBitmap = BitmapFactory.decodeStream(connection.inputStream)
                }
                emitter.onNext(brand)

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
        return obs

    }


    fun getBrands(): Observable<Array<SmartphoneBrand>> {
        val observable = Observable.create<Array<SmartphoneBrand>> { emitter ->
            try {
                var connection = connect("${BASE_URL}/brands")
                val gson = GsonBuilder().create()
                val brands = gson.fromJson(
                    InputStreamReader(connection.inputStream),
                    Array<SmartphoneBrand>::class.java

                )
                brands.forEach() {
                    connection = connect("${BASE_URL}/${it.logo}")
                    it.imageBitmap = BitmapFactory.decodeStream(connection.inputStream)

                }
                emitter.onNext(brands)

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
        return observable

    }

    fun getPhones(brandId: Int): Observable<Array<Phone>> {
        val observable = Observable.create<Array<Phone>> { emitter ->
            try {
                var connection = connect("${BASE_URL}/brands/${brandId}/phones")
                val gson = GsonBuilder().create()
                val phones = gson.fromJson(
                    InputStreamReader(connection.inputStream),
                    Array<Phone>::class.java

                )
                emitter.onNext(phones)

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
        return observable
    }

}