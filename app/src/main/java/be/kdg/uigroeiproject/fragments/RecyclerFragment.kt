package be.kdg.uigroeiproject.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.adapters.BrandAdapter
import be.kdg.uigroeiproject.model.SmartphoneBrand
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class RecyclerFragment : Fragment() {
    private lateinit var listener: BrandAdapter.BrandSelectionListener
    private lateinit var disposable: Disposable
//    private lateinit var brands: Array<SmartphoneBrand>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BrandAdapter.BrandSelectionListener)
            listener = context
        else
            throw Exception()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_recycler, container, false)

            disposable = RestClient(requireActivity().applicationContext)
                .getBrands()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { brands ->
                    view.findViewById<RecyclerView>(R.id.rvData).apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = BrandAdapter(brands, context, listener)
                    }
                }
        return view
    }


    override fun onDestroy() {
        if (::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }


}
