package be.kdg.uigroeiproject.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import be.kdg.uigroeiproject.R
import be.kdg.uigroeiproject.activities.PhonesRecycler
import be.kdg.uigroeiproject.adapters.BrandAdapter
import be.kdg.uigroeiproject.adapters.PhonesAdapter
import be.kdg.uigroeiproject.model.SmartphoneBrand
import be.kdg.uigroeiproject.model.convertDate
import be.kdg.uigroeiproject.model.getData
import be.kdg.uigroeiproject.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {
    private lateinit var logo: ImageView
    private lateinit var brandName: TextView
    private lateinit var foundingDate: TextView
    private lateinit var yearsService: TextView
    private lateinit var phonesButton: Button
    private lateinit var disposable: Disposable
    var index: Int = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_main, container, false)

        logo = view.findViewById(R.id.imgLogo)
        brandName = view.findViewById(R.id.brand_value)
        foundingDate = view.findViewById(R.id.date_value)
        yearsService = view.findViewById(R.id.years_value)
        phonesButton = view.findViewById(R.id.phones_button)

        updateFields()
        return view
    }


    private fun updateFields() {
        disposable = RestClient(requireContext())
            .getBrand(index)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { brand ->
                this.logo.setImageBitmap(brand.imageBitmap)
                this.brandName.text = brand.brand_name
                this.foundingDate.text = brand.founding_date
                this.yearsService.text = brand.yearsService.toString()
                this.phonesButton.setOnClickListener() {
                    val phonesIntent = Intent(requireContext(), PhonesRecycler::class.java)
                    phonesIntent.putExtra("currentIndex", index)
                    startActivity(phonesIntent)
                }
            }

    }


    fun setBrandIndex(brandIndex: Int) {
        index = brandIndex
        updateFields()
    }

    override fun onDestroy() {
        if (::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }

}
